package net.genflowstudios.globalstats.mysql;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.UUID;

import net.genflowstudios.globalstats.config.ConfigManager;
import net.genflowstudios.kingdoms.player.KingdomPlayer;

import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

public class MySQLAdapter{

	public static Connection connection;
	

	// Opens the MySQL Connection
	public synchronized static void openConnection() {
		FileConfiguration cfg = ConfigManager.getDefaultConfig();
		String host = cfg.getConfigurationSection("MySQL").getString("Host-IP");
		String port = cfg.getConfigurationSection("MySQL").getString("Port");
		String database = cfg.getConfigurationSection("MySQL").getString("Database");
		String username = cfg.getConfigurationSection("MySQL").getString("Username");
		String password = cfg.getConfigurationSection("MySQL").getString("Password");
		try{
			connection = DriverManager.getConnection("jdbc:mysql://"+host + ":" + port+"/"+database, username, password);
		} catch(Exception e){
			e.printStackTrace();
		}
	}
	
	/**
	 * Creates default mysql tables.
	 */
	public synchronized static void createTables(){
		try{
			if(connection == null){
				openConnection();
				PreparedStatement sql = connection.prepareStatement("CREATE TABLE IF NOT EXISTS playerstats(id int NOT NULL AUTO_INCREMENT, Player varchar(30) NOT NULL, Bonds double NOT NULL, Tokens int NOT NULL, Level int NOT NULL, Exp double NOT NULL, HarvestTool varchar(6) NOT NULL, Area varchar(255) NOT NULL, SocialRank varchar(255), MilitaryRank varchar(255), PRIMARY KEY(id))");
				sql.executeUpdate();
			}else{
				openConnection();
				PreparedStatement sql = connection.prepareStatement("CREATE TABLE IF NOT EXISTS playerstats(id int NOT NULL AUTO_INCREMENT, Player varchar(30) NOT NULL, Bonds double NOT NULL, Tokens int NOT NULL, Level int NOT NULL, Exp double NOT NULL, HarvestTool varchar(6) NOT NULL, Area varchar(255) NOT NULL, SocialRank varchar(255), MilitaryRank varchar(255), PRIMARY KEY(id))");
				sql.executeUpdate();
			}
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			closeConnection();
		}
	}

	// Closes the MySQL Connection
	public synchronized static void closeConnection() {
		try{
			connection.close();
		} catch(Exception e){
			e.printStackTrace();
		}
	}


	//Checks if the player data is already created.
	public synchronized static boolean playerDataContainsPlayer(UUID uuid){
		try{
			openConnection();
			PreparedStatement sql = connection.prepareStatement("SELECT * FROM playerstats WHERE player=?");
			sql.setString(1, Bukkit.getOfflinePlayer(uuid).getName());
			ResultSet resultSet = sql.executeQuery();
			boolean containsPlayer = resultSet.next();
			sql.close();
			resultSet.close();
			return containsPlayer;
		}catch(Exception e){
			e.printStackTrace();
			return false;
		}finally{
			closeConnection();
		}
	}

	public synchronized static void updateData(UUID uuid){
		KingdomPlayer kingdomPlayer = new KingdomPlayer(uuid);
		try{
			openConnection();
			PreparedStatement sql = connection.prepareStatement("UPDATE playerstats SET Bonds=?, Tokens=?, Area=?, Level=?, Exp=? WHERE Player=?");
			sql.setDouble(1, kingdomPlayer.getStats().getBonds());
			sql.setInt(2, kingdomPlayer.getStats().getTokens());
			sql.setString(3, kingdomPlayer.getLocation().getArea());
			sql.setInt(4, 1);
			sql.setDouble(5, 0.0);
			sql.setString(6, Bukkit.getOfflinePlayer(uuid).getName());
			sql.executeUpdate();
			sql.close();
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			closeConnection();
		}
	}

	public synchronized static void createData(UUID uuid){
		KingdomPlayer kingdomPlayer = new KingdomPlayer(uuid);
		try{
			openConnection();
			PreparedStatement sql = connection.prepareStatement("INSERT INTO playerstats(Player, Bonds, Tokens, Level, Exp, HarvestTool, Area) values(?, ?, ?, ?, ?, ?, ?)");
			sql.setString(1, Bukkit.getOfflinePlayer(uuid).getName());
			sql.setDouble(2, kingdomPlayer.getStats().getBonds());
			sql.setInt(3, kingdomPlayer.getStats().getTokens());
			sql.setInt(4, 1);
			sql.setDouble(5, 0.0);
			sql.setString(6, "false");
			sql.setString(7, kingdomPlayer.getLocation().getArea());
			sql.executeUpdate();
			sql.close();
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			closeConnection();
		}
	}

}
