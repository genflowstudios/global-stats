package net.genflowstudios.globalstats.listeners;

import java.util.ArrayList;

import net.genflowstudios.globalstats.Engine;
import net.genflowstudios.globalstats.mysql.MySQLAdapter;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class ServerJoinListener implements Listener{

	public static ArrayList<Player> newPlayers = new ArrayList<Player>();
	
	@EventHandler(priority = EventPriority.LOW)
	public void onServerJoin(PlayerJoinEvent event){
		final Player player = event.getPlayer();
		Bukkit.getScheduler().scheduleSyncDelayedTask(Engine.getInstance(), new Runnable(){
			@Override
			public void run(){
				if(MySQLAdapter.playerDataContainsPlayer(player.getUniqueId())){
					MySQLAdapter.updateData(player.getUniqueId());
				}else{
					MySQLAdapter.createData(player.getUniqueId());
				}
			}
		});
	}
	
	@EventHandler(priority = EventPriority.MONITOR)
	public void onServerQuit(PlayerQuitEvent event){
		final Player player = event.getPlayer();
		Bukkit.getScheduler().scheduleSyncDelayedTask(Engine.getInstance(), new Runnable(){
			@Override
			public void run(){
				if(MySQLAdapter.playerDataContainsPlayer(player.getUniqueId())){
					MySQLAdapter.updateData(player.getUniqueId());
				}else{
					MySQLAdapter.createData(player.getUniqueId());
				}
			}
		});
		
	}
	
	
}
