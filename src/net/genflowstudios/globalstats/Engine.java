package net.genflowstudios.globalstats;

import java.io.File;

import net.genflowstudios.globalstats.config.ConfigChangeHandler;
import net.genflowstudios.globalstats.config.ConfigManager;
import net.genflowstudios.globalstats.config.SettingsHandler;
import net.genflowstudios.globalstats.listeners.ServerJoinListener;
import net.genflowstudios.globalstats.mysql.MySQLAdapter;

import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

public class Engine extends JavaPlugin{


	private static Engine instance;

	// Allows access to the main class
	public static Engine getInstance() {
		return instance;
	}


	@Override
	public void onEnable(){
		super.onEnable();
		instance = this;
		CommandManager commandManager = new CommandManager(this);
		new EventManager(this);
		Bukkit.getPluginManager().registerEvents(new ServerJoinListener(), this);
		getCommand("Stats").setExecutor(commandManager);
		getCommand("Bonds").setExecutor(commandManager);
		getCommand("Tokens").setExecutor(commandManager);
		checkFiles();
		MySQLAdapter.createTables();
		getLogger().info("has been enabled!");
	}


	@Override
	public void onDisable(){
		super.onDisable();
		getLogger().info("has been disabled!");
	}

	// Checks default files.
	private void checkFiles() {
		File statsFiles = new File(getInstance().getDataFolder(), "Settings");
		FileConfiguration cfg = ConfigManager.getDefaultConfig();
		new SettingsHandler();
		//If default config files exist.
		if(statsFiles.exists()){
			
		}else{
			createDefaultSettings(cfg);
		}
	}

	//Creates default settings.
	private void createDefaultSettings(FileConfiguration cfg){
		ConfigChangeHandler configChangeHandler = new ConfigChangeHandler();
		configChangeHandler.generateSettingsFile(cfg);
		getLogger().info("Created default config files!");
	}
	
	

}
