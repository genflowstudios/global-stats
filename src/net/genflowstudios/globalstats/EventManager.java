package net.genflowstudios.globalstats;

import net.genflowstudios.globalstats.listeners.ServerJoinListener;

import org.bukkit.Bukkit;

public class EventManager{

	Engine engine;
	
	
	public EventManager(Engine engine){
		this.engine = engine;
	}

	public void registerEvents(){
		Bukkit.getPluginManager().registerEvents(new ServerJoinListener(), engine);
	}

}
