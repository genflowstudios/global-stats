package net.genflowstudios.globalstats.config;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import net.genflowstudios.globalstats.Engine;
import net.genflowstudios.globalstats.economy.EconData;

import org.bukkit.Location;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

public class ConfigChangeHandler{

	static Engine kingdoms;


	

	/**
	 * @param The xChunk coordinate.
	 * @param The zChunk coordinate.
	 * @return The default claimed chunks.
	 */
	public static List<String> getDefaultClaimedChunks(int xChunk, int zChunk){
		List<String> defaultChunks = new ArrayList<String>();
		String cityCenter = xChunk + ", " + zChunk;
		int plusXChunk = xChunk + 1;
		int minusXChunk = xChunk - 1;
		int plusZChunk = zChunk + 1;
		int minusZChunk = zChunk - 1;
		String cityNorth = plusXChunk + ", " + zChunk;
		String citySouth = minusXChunk + ", " + zChunk;
		String cityEast = xChunk + ", " + plusZChunk;
		String cityWest = xChunk + ", " + minusZChunk;
		defaultChunks.add(cityCenter);
		defaultChunks.add(cityNorth);
		defaultChunks.add(citySouth);
		defaultChunks.add(cityEast);
		defaultChunks.add(cityWest);
		return defaultChunks;
	}

	/**
	 * @param The player.
	 */
	public void generatePlayerConfig(UUID uuid, Player player, FileConfiguration cfgPlayer){
		Location playerLocation = player.getLocation();
		int x = playerLocation.getBlockX();
		int y = playerLocation.getBlockY();
		int z = playerLocation.getBlockZ();
		int xChunk = playerLocation.getChunk().getX();
		int zChunk = playerLocation.getChunk().getZ();
		List<String> settlements = new ArrayList<String>();
		List<String> cities = new ArrayList<String>();
		cfgPlayer.createSection("Stats");
		cfgPlayer.createSection("Settings");
		cfgPlayer.createSection("Location");
		cfgPlayer.createSection("Owned-Areas");
		cfgPlayer.getConfigurationSection("Owned-Areas").set("Settlements", settlements);
		cfgPlayer.getConfigurationSection("Owned-Areas").set("Cities", cities);
		cfgPlayer.getConfigurationSection("Stats").createSection("Race/Class");
		cfgPlayer.getConfigurationSection("Stats").createSection("Kingdom-Stats");
		cfgPlayer.getConfigurationSection("Stats").createSection("Player-Stats");
		cfgPlayer.getConfigurationSection("Stats").createSection("Race/Class");
		cfgPlayer.getConfigurationSection("Stats").createSection("Social/Military");
		cfgPlayer.getConfigurationSection("Stats").getConfigurationSection("Player-Stats").set("Bonds", 0.0);
		cfgPlayer.getConfigurationSection("Stats").getConfigurationSection("Player-Stats").set("Tokens", 0);
		cfgPlayer.getConfigurationSection("Stats").getConfigurationSection("Player-Stats").set("Level", 1);
		cfgPlayer.getConfigurationSection("Stats").getConfigurationSection("Player-Stats").set("Kills", 0);
		cfgPlayer.getConfigurationSection("Stats").getConfigurationSection("Player-Stats").set("Deaths", 0);
		cfgPlayer.getConfigurationSection("Stats").getConfigurationSection("Kingdom-Stats").set("Kingdom", "**");
		cfgPlayer.getConfigurationSection("Stats").getConfigurationSection("Kingdom-Stats").set("Guild", "**");
		cfgPlayer.getConfigurationSection("Stats").getConfigurationSection("Race/Class").set("Race", "**");
		cfgPlayer.getConfigurationSection("Stats").getConfigurationSection("Race/Class").set("Class", "**");
		cfgPlayer.getConfigurationSection("Stats").getConfigurationSection("Race/Class").set("Specialty", "**");
		cfgPlayer.getConfigurationSection("Stats").getConfigurationSection("Social/Military").set("Social-Rank", "Peasant");
		cfgPlayer.getConfigurationSection("Stats").getConfigurationSection("Social/Military").set("Quest-Points", 0);
		cfgPlayer.getConfigurationSection("Stats").getConfigurationSection("Social/Military").set("Military-Rank", "**");
		cfgPlayer.getConfigurationSection("Stats").getConfigurationSection("Social/Military").set("Badges", 0);
		cfgPlayer.getConfigurationSection("Settings").set("Harvest-Tool", false);
		cfgPlayer.getConfigurationSection("Settings").set("Scoreboard-Mode", false);
		cfgPlayer.getConfigurationSection("Settings").set("Build-Tool", false);
		cfgPlayer.getConfigurationSection("Location").set("Area", "Wilderness");
		cfgPlayer.getConfigurationSection("Location").set("x", x);
		cfgPlayer.getConfigurationSection("Location").set("y", y);
		cfgPlayer.getConfigurationSection("Location").set("z", z);
		cfgPlayer.getConfigurationSection("Location").set("xChunk", xChunk);
		cfgPlayer.getConfigurationSection("Location").set("zChunk", zChunk);
		cfgPlayer.getConfigurationSection("Location").set("Chunks", xChunk + ", " + zChunk);
		ConfigManager.savePlayerConfig(uuid, cfgPlayer);
	}

	/**
	 * @param The player.
	 */
	public void savePlayerConfigs(Player player){
		FileConfiguration cfg = ConfigManager.getPlayerConfig(player.getUniqueId());
		savePlayerBonds(player, cfg);
		savePlayerTokens(player, cfg);
		savePlayerLevel(player, cfg);
		savePlayerLocation(player, cfg);
	}
	

	/**
	 * @param The player.
	 * @param The file configuaration.
	 */
	public void savePlayerLocation(Player player, FileConfiguration cfg){
		Location location = player.getLocation();
		int x = location.getBlockX();
		int y = location.getBlockY();
		int z = location.getBlockZ();
		int xChunk = location.getChunk().getX();
		int zChunk = location.getChunk().getZ();
		cfg.getConfigurationSection("Location").set("x", x);
		cfg.getConfigurationSection("Location").set("y", y);
		cfg.getConfigurationSection("Location").set("z", z);
		cfg.getConfigurationSection("Location").set("xChunk", xChunk);
		cfg.getConfigurationSection("Location").set("zChunk", zChunk);
		cfg.getConfigurationSection("Location").set("Chunks", xChunk + ", " + zChunk);
		ConfigManager.savePlayerConfig(player.getUniqueId(), cfg);
	}


	/**
	 * @param The player.
	 * @param The file configuration.
	 */
	public void savePlayerLevel(Player player, FileConfiguration cfg){
		//Waiting for leveling implementation.
	}

	/**
	 * @param The player.
	 * @param The file configuration.
	 */
	public void savePlayerTokens(Player player, FileConfiguration cfg){
		EconData econData = new EconData();
		cfg.getConfigurationSection("Stats").getConfigurationSection("Player-Stats").set("Tokens", econData.getTokens(player.getUniqueId()));
		ConfigManager.savePlayerConfig(player.getUniqueId(), cfg);
	}

	/**
	 * @param The player.
	 * @param The file configuration.
	 */
	public void savePlayerBonds(Player player, FileConfiguration cfg){
		EconData econData = new EconData();
		cfg.getConfigurationSection("Stats").getConfigurationSection("Player-Stats").set("Bonds", econData.getMoney(player.getUniqueId()));
		ConfigManager.savePlayerConfig(player.getUniqueId(), cfg);
	}

	/**
	 * @param The file configuration.
	 */
	public void generateSettingsFile(FileConfiguration cfg){
		cfg.createSection("Settings");
		cfg.getConfigurationSection("Settings").set("Flatfile", true);
		cfg.getConfigurationSection("Settings").createSection("Gameplay-Settings");
		cfg.getConfigurationSection("Settings").createSection("Plugin-Hook-Settings");
		cfg.getConfigurationSection("Settings").getConfigurationSection("Gameplay-Settings").createSection("Kingdom-Settings");
		cfg.getConfigurationSection("Settings").getConfigurationSection("Gameplay-Settings").getConfigurationSection("Kingdom-Settings").set("Kingdoms-Enabled", false);
		cfg.getConfigurationSection("Settings").getConfigurationSection("Gameplay-Settings").getConfigurationSection("Kingdom-Settings").set("Cities-Enabled", true);
		cfg.getConfigurationSection("Settings").getConfigurationSection("Gameplay-Settings").getConfigurationSection("Kingdom-Settings").set("Parties-Enabled", true);
		cfg.getConfigurationSection("Settings").getConfigurationSection("Gameplay-Settings").getConfigurationSection("Kingdom-Settings").set("Social-Ranks-Enabled", true);
		cfg.getConfigurationSection("Settings").getConfigurationSection("Gameplay-Settings").getConfigurationSection("Kingdom-Settings").set("Military-Ranks-Enabled", true);
		cfg.getConfigurationSection("Settings").getConfigurationSection("Plugin-Hook-Settings").set("Vault-Enabled", true);
		cfg.getConfigurationSection("Settings").getConfigurationSection("Plugin-Hook-Settings").set("Kingdoms-Enabled", true);
		cfg.createSection("MySQL");
		cfg.getConfigurationSection("MySQL").set("Host-IP", "localhost");
		cfg.getConfigurationSection("MySQL").set("Database", "kingdoms");
		cfg.getConfigurationSection("MySQL").set("Port", "3306");
		cfg.getConfigurationSection("MySQL").set("Username", "root");
		cfg.getConfigurationSection("MySQL").set("Password", "pass");
		ConfigManager.saveDefaultConfig(cfg);
	}


}
