package net.genflowstudios.globalstats.config;

import net.genflowstudios.globalstats.Engine;

import org.bukkit.configuration.file.FileConfiguration;

public class SettingsHandler{

	Engine kingdoms;

	
	public boolean isFlatfileMode(){
		FileConfiguration cfg = ConfigManager.getDefaultConfig();
		return cfg.getConfigurationSection("Settings").getBoolean("Flatfile");
	}
	
	public String getPluginMode(){
		FileConfiguration cfg = ConfigManager.getDefaultConfig();
		return cfg.getConfigurationSection("Settings").getConfigurationSection("Gameplay-Settings").getString("Plugin-Mode");
	}
	
	public boolean isGlobalEconomyEnabled(){
		FileConfiguration cfg = ConfigManager.getDefaultConfig();
		return cfg.getConfigurationSection("Settings").getConfigurationSection("Plugin-Hook-Settings").getBoolean("Global-Economy-Enabled");
	}
	
	public boolean isVaultEnabled(){
		FileConfiguration cfg = ConfigManager.getDefaultConfig();
		return cfg.getConfigurationSection("Settings").getConfigurationSection("Plugin-Hook-Settings").getBoolean("Vault-Enabled");
	}
	
	public boolean isSocialRanksEnabled(){
		FileConfiguration cfg = ConfigManager.getDefaultConfig();
		return cfg.getConfigurationSection("Settings").getConfigurationSection("Gameplay-Settings").getConfigurationSection("Free-Roam-Settings").getBoolean("Social-Ranks-Enabled");
	}
	
	public boolean isMilitaryRanksEnabled(){
		FileConfiguration cfg = ConfigManager.getDefaultConfig();
		return cfg.getConfigurationSection("Settings").getConfigurationSection("Gameplay-Settings").getConfigurationSection("Free-Roam-Settings").getBoolean("Military-Ranks-Enabled");
	}

	
	
	
}
